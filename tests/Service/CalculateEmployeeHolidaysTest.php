<?php

namespace App\Tests\Service;

use App\Entity\Employees;
use App\Service\CalculateEmployeeHolidays;
use Mockery;
use PHPUnit\Framework\TestCase;

class CalculateEmployeeHolidaysTest extends TestCase
{
    public function testCalculateHolidayUseContractOverrideValue(): void
    {
        $employee = Mockery::mock(Employees::class);
        $employee->shouldReceive('getVacationDayContractOverride')->once()->andReturn(35);
        $employee->name = 'Test name';
        $employee->vacation_day_contract_override = 35;

        $calculateEmployeeHoliday = new CalculateEmployeeHolidays('employees');

        $this->assertEquals(35, $calculateEmployeeHoliday->processCalculationOnEmployee($employee));
    }

    public function testCalculateHolidayOver30Years(): void
    {
        $startDate = date('m.d.Y', strtotime('-1 years', strtotime(date_create()->format('m.d.Y'))));

        $employee = Mockery::mock(Employees::class);
        $employee->shouldReceive('getVacationDayContractOverride')->once()->andReturn(null);
        $employee->shouldReceive('getBirthDate')->once()->andReturn('01.01.1990');
        $employee->shouldReceive('getStartDate')->once()->andReturn($startDate);
        $employee->name = 'Test name';
        $employee->birth_date = '01.01.1990';
        $employee->start_date = $startDate;
        $employee->vacation_day_contract_override = null;

        $calculateEmployeeHoliday = new CalculateEmployeeHolidays('employees');

        $this->assertEquals(30, $calculateEmployeeHoliday->processCalculationOnEmployee($employee));
    }

    public function testCalculateHolidayOver30YearsIncludeExtra(): void
    {
        $startDate = date('m.d.Y', strtotime('-6 years', strtotime(date_create()->format('m.d.Y'))));

        $employee = Mockery::mock(Employees::class);
        $employee->shouldReceive('getVacationDayContractOverride')->once()->andReturn(null);
        $employee->shouldReceive('getBirthDate')->once()->andReturn('01.01.1990');
        $employee->shouldReceive('getStartDate')->once()->andReturn($startDate);
        $employee->name = 'Test name';
        $employee->birth_date = '01.01.1990';
        $employee->start_date = $startDate;
        $employee->vacation_day_contract_override = null;

        $calculateEmployeeHoliday = new CalculateEmployeeHolidays('employees');
        $this->assertEquals(31, $calculateEmployeeHoliday->processCalculationOnEmployee($employee));
    }

    public function testCalculateHolidayUnder30Years(): void
    {
        $birthDate = date('m.d.Y', strtotime('20 years', strtotime(date_create()->format('m.d.Y'))));
        $startDate = date('m.d.Y', strtotime('-1 years', strtotime(date_create()->format('m.d.Y'))));

        $employee = Mockery::mock(Employees::class);
        $employee->shouldReceive('getVacationDayContractOverride')->once()->andReturn(null);
        $employee->shouldReceive('getBirthDate')->once()->andReturn($birthDate);
        $employee->shouldReceive('getStartDate')->once()->andReturn($startDate);
        $employee->name = 'Test name';
        $employee->birth_date = $birthDate;
        $employee->start_date = $startDate;
        $employee->vacation_day_contract_override = null;

        $calculateEmployeeHoliday = new CalculateEmployeeHolidays('employees');

        $this->assertEquals(26, $calculateEmployeeHoliday->processCalculationOnEmployee($employee));
    }

    public function testCalculateHolidayUnder30YearsIncludeExtra(): void
    {
        $birthDate = date('m.d.Y', strtotime('20 years', strtotime(date_create()->format('m.d.Y'))));
        $startDate = date('m.d.Y', strtotime('-6 years', strtotime(date_create()->format('m.d.Y'))));

        $employee = Mockery::mock(Employees::class);
        $employee->shouldReceive('getVacationDayContractOverride')->once()->andReturn(null);
        $employee->shouldReceive('getBirthDate')->once()->andReturn($birthDate);
        $employee->shouldReceive('getStartDate')->once()->andReturn($startDate);
        $employee->name = 'Test name';
        $employee->birth_date = $birthDate;
        $employee->start_date = $startDate;
        $employee->vacation_day_contract_override = null;

        $calculateEmployeeHoliday = new CalculateEmployeeHolidays('employees');

        $this->assertEquals(27, $calculateEmployeeHoliday->processCalculationOnEmployee($employee));
    }

    public function testCalculateHolidayOver30YearsIncludeExtra2(): void
    {
        $startDate = date('m.d.Y', strtotime('-16 years', strtotime(date_create()->format('m.d.Y'))));

        $employee = Mockery::mock(Employees::class);
        $employee->shouldReceive('getVacationDayContractOverride')->once()->andReturn(null);
        $employee->shouldReceive('getBirthDate')->once()->andReturn('1.1.1990');
        $employee->shouldReceive('getStartDate')->once()->andReturn($startDate);
        $employee->name = 'Test name';
        $employee->birth_date = '1.1.1990';
        $employee->start_date = $startDate;
        $employee->vacation_day_contract_override = null;

        $calculateEmployeeHoliday = new CalculateEmployeeHolidays('employees');

        $this->assertEquals(33, $calculateEmployeeHoliday->processCalculationOnEmployee($employee));
    }
}
