# CodeTest

This application is based on Symfony console components with doctrine packages to
configure connection to DB and entity mappings.

To be able to run script you have to:
- install composer packages
- install symfony CLI (curl https://get.symfony.com/cli/installer | bash)

Command can be executed with `symfony console app:get-holidays`

Command will accept parameters for: 
- output formats (json - default, table) 
  - `symfony console app:get-holidays table`
- sorting output data (employees - default, vacations) 
  - `symfony console app:get-holidays table vacations`

Unit tests:
- under `tests` directory
- configuration file `codetest/phpunit.xml`
- bootstrap file `codetest/tests/bootstrap.php`
