<?php

namespace App\Command;

use App\Entity\Employees;
use App\Service\CalculateEmployeeHolidays;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetEmployeeHolidaysCommand extends Command
{
    const OUTPUT_FORMAT_JSON = 'json';
    const OUTPUT_FORMAT_TABLE = 'table';

    protected static $defaultName = 'app:get-holidays';

    /** @var EntityManagerInterface  */
    private EntityManagerInterface $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setDescription('Getting employee vacation days')
            ->addArgument('format', InputArgument::OPTIONAL, 'Output format', self::OUTPUT_FORMAT_JSON)
            ->addArgument('sorting', InputArgument::OPTIONAL, 'Sort output', CalculateEmployeeHolidays::SORT_BY_EMPLOYEE_NAME);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $outputFormat = ($input->getArgument('format'));
        $sortBy = ($input->getArgument('sorting'));

        $employeesRepo = $this->entityManager->getRepository(Employees::class);
        /**
         * For sake of this coding test, I kept it simple and just fetch all employees to be processed at a same time.
         * But this might get tricky on big amounts of data and if some other operations would be necessary with DB.
         * As an alternative I would introduce processing employees in batches. Count total employees, split them in to
         * pages (i.e. by 1000 records) and then process each batch separately.
         */
        $employees = $employeesRepo->findAll();

        $calculateEmployeeHoliday = new CalculateEmployeeHolidays($sortBy);
        $results = $calculateEmployeeHoliday->calculateHoliday($employees);

        if ($outputFormat === self::OUTPUT_FORMAT_TABLE) {
            $resultsArray = [];
            foreach ($results as $result) {
                $resultsArray[] = $result->toArray();
            }

            $table = new Table($output);
            $table
                ->setHeaders(['Employee name', 'Vacation days'])
                ->setRows($resultsArray)
            ;
            $table->render();

            dd();
        } else {
            dd(json_encode($results));
        }
    }
}
