<?php

namespace App\Service;

use App\Entity\Employees;
use ValueObjects\EmployeeHolidaysValueObject;

class CalculateEmployeeHolidays
{
    const DEFAULT_HOLIDAY_DURATION = 26;
    const DEFAULT_HOLIDAY_AFTER_30_DURATION = 30;
    const AGE_30_YEARS = 30;
    const ADDITIONAL_VACATION_DAY_INTERVAL = 5;
    const SORT_BY_EMPLOYEE_NAME = 'employees';
    const SORT_BY_VACATION_DAYS = 'vacations';

    /** @var string */
    protected string $sortBy;

    /**
     * @param string $sortBy
     */
    public function __construct(string $sortBy)
    {
        $this->sortBy = $sortBy;
    }

    /**
     * @param Employees[] $employees
     * @return array
     */
    public function calculateHoliday(array $employees): array
    {
        $results = [];
        foreach ($employees as $employee) {
            $employeeHolidayObject = new EmployeeHolidaysValueObject();
            $employeeHolidayObject->name = $employee->getName();
            $employeeHolidayObject->holidayDuration = $this->processCalculationOnEmployee($employee);
            $results[] = $employeeHolidayObject;
        }

        if ($this->sortBy === self::SORT_BY_VACATION_DAYS) {
            usort($results, fn($fist, $second) => strcmp($fist->holidayDuration, $second->holidayDuration));
        } else {
            usort($results, fn($fist, $second) => strcmp($fist->name, $second->name));
        }

        return $results;
    }

    /**
     * @param Employees $employee
     * @return int
     */
    public function processCalculationOnEmployee(Employees $employee): int
    {
        $employeeHolidayDuration = self::DEFAULT_HOLIDAY_DURATION;
        $employeeContractOverride = $employee->getVacationDayContractOverride();

        if ($employeeContractOverride !== null) {
            return $employeeContractOverride;
        }

        $age = $this->getYearsDiffFromDate($employee->getBirthDate());
        if ($age >= self::AGE_30_YEARS) {
            $employeeHolidayDuration = self::DEFAULT_HOLIDAY_AFTER_30_DURATION;
        }

        $startDate = $this->getYearsDiffFromDate($employee->getStartDate());
        if ($startDate >= self::ADDITIONAL_VACATION_DAY_INTERVAL) {
            $employeeHolidayDuration = $employeeHolidayDuration + (int)($startDate / self::ADDITIONAL_VACATION_DAY_INTERVAL);
        }

        return $employeeHolidayDuration;
    }

    /**
     * @param string $date
     * @return int
     */
    public function getYearsDiffFromDate(string $date):int
    {
        $date = explode(".", $date);
        return (date("md", date("U", mktime(0, 0, 0, $date[0], $date[1], $date[2]))) > date("md")
            ? ((date("Y") - $date[2]) - 1)
            : (date("Y") - $date[2]));
    }
}
