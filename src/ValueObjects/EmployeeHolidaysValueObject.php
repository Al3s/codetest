<?php

namespace ValueObjects;

class EmployeeHolidaysValueObject
{
    /** @var string */
    public string $name;

    /** @var int */
    public int $holidayDuration;

    /**
     * Serialize to array if needed
     *
     * @return array
     */
    public function toArray(): array {
        return [
            'name' => $this->name,
            'holidayDuration' => $this->holidayDuration,
        ];
    }
}
