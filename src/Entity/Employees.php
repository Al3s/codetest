<?php

namespace App\Entity;

use App\Repository\EmployeesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmployeesRepository::class)
 */
class Employees
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="text")
     */
    private string $id;

    /**
     * @ORM\Column(type="text")
     */
    public string $name;

    /**
     * @ORM\Column(type="text")
     */
    public string $birth_date;

    /**
     * @ORM\Column(type="text")
     */
    public string $start_date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    public ?int $vacation_day_contract_override;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBirthDate(): ?string
    {
        return $this->birth_date;
    }

    public function setBirthDate(string $birth_date): self
    {
        $this->birth_date = $birth_date;

        return $this;
    }

    public function getStartDate(): ?string
    {
        return $this->start_date;
    }

    public function setStartDate(string $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getVacationDayContractOverride(): ?int
    {
        return $this->vacation_day_contract_override;
    }

    public function setVacationDayContractOverride(?int $vacation_day_contract_override): self
    {
        $this->vacation_day_contract_override = $vacation_day_contract_override;

        return $this;
    }
}
